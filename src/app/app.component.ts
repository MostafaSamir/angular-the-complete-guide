import { Recipe } from './recipes/recipe.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project';

  recipe = false;
  shopping = false;

  RecipeClick(val : boolean)
  {
    this.recipe = val;
  }

  ShoppingClick(val : boolean)
  {
    this.shopping = val;
  }
}
