import { Component ,Output, EventEmitter } from "@angular/core";

@Component({
  selector:'app-header',
  templateUrl:'./header-component.html',

})

export class HeaderComponent
{
  @Output() RecipesState = new EventEmitter<boolean>();
  @Output() ShoppingState = new EventEmitter<boolean>();

  recipe = false;
  shopping = false;
  isNavbarCollapsed=true;
  isClicked = false;

    cccc1()
    {
      this.isClicked = !this.isClicked;
    }

}


