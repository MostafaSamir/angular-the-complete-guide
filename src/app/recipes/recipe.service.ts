import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { EventEmitter, Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { Recipe } from "./recipe.model";
@Injectable()
export class RecipeService {

  constructor(private shoppingListService : ShoppingListService) {


  }

  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe('Carrot Meat with Rice',
               '',
               'https://www.thespruceeats.com/thmb/cO72JFFH0TCAufENSxUfqE8TmKw=/450x0/filters:no_upscale():max_bytes(150000):strip_icc()/vegan-tofu-tikka-masala-recipe-3378484-hero-01-d676687a7b0a4640a55be669cba73095.jpg',
                [
                  new Ingredient('Meat',1),
                  new Ingredient('Rice',20),
                  new Ingredient('Carrot',5)
                ]),
    new Recipe('Spicy Chicken',
               'Very Special',
               'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=768,574',
               [
                new Ingredient('Pepper',4),
                new Ingredient('Chicken',2)
              ]),
    new Recipe('Lemon Potato',
               'Delicious',
               'https://www.simplyrecipes.com/thmb/OCi18J2V8OeKDFV3FxoeKvgq74E=/1423x1067/smart/filters:no_upscale()/__opt__aboutcom__coeus__resources__content_migration__simply_recipes__uploads__2012__07__grilled-sweet-potatoes-horiz-a-1600-7c8292daa98e4020b447f0dc97a45cb7.jpg',
               [
                new Ingredient('Lemon',3),
                new Ingredient('Potato',2)
              ])

              ];

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number){
    return this.recipes[index];
  }

  addIngredientsToShoppingList (ingredients: Ingredient[])
  {
    this.shoppingListService.addMultiIngredients(ingredients);
  }

}
