import { RecipeService } from './../recipe.service';
import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Recipe } from '../recipe.model';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipes-detail',
  templateUrl: './recipes-detail.component.html',
  styleUrls: ['./recipes-detail.component.css']
})
export class RecipesDetailComponent implements OnInit {

  @ViewChild('togg', {static: true}) togg: ElementRef;
  @Input() recipe : Recipe;

  isClicked = false;
  id: number;
  constructor(private recipeService : RecipeService,
              private router:Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
      this.route.params
        .subscribe(
          (params:Params) => {
            this.id = +params['id'];
            this.recipe = this.recipeService.getRecipe(this.id);
          }
        )
  }

  cccc()
  {
    this.isClicked = !this.isClicked;
    if(this.isClicked){
    this.togg.nativeElement.classList.add('show');
    }
    else{
      this.togg.nativeElement.classList.remove('show');
    }
  }

  onAddToShoppingList() {
    this.recipeService.addIngredientsToShoppingList(this.recipe.ingredients);
  }
  onEditRecipe(){
    this.router.navigate(['edit'],{relativeTo:this.route});
  }

}
