import { EventEmitter } from '@angular/core';
import { Ingredient } from "../shared/ingredient.model";


export class ShoppingListService {

  ingredientChanged = new EventEmitter<Ingredient[]>();

    private ingredients : Ingredient[] = [
    new Ingredient('Apples',5),
    new Ingredient('Tomatoes',10),
  ];

  getIngredients() {
    return this.ingredients;
  }

  addIngredients(ingredinet:Ingredient){
    this.ingredients.push(ingredinet);
    this.ingredientChanged.emit(this.ingredients.slice());
  }

  addMultiIngredients( ingredients : Ingredient[] )
  {
    this.ingredients.push(...ingredients);
    this.ingredientChanged.emit(this.ingredients.slice());
  }
}
